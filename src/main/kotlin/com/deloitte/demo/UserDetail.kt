package com.deloitte.demo

import com.fasterxml.jackson.annotation.JsonFormat
import java.util.Date

class UserDetail (
        val username: String,
        val fullName: String,
        val gender: String,
        val age: Int,
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
        val birthday: Date
)