package com.deloitte.demo

import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.net.URI
import java.text.SimpleDateFormat

@RestController
class DemoController {

    @RequestMapping("/")
    fun index(): String = "Greetings from Spring Boot!"

    @PostMapping(path = arrayOf("/custom-response"),
            consumes = arrayOf(MediaType.APPLICATION_JSON_VALUE),
            produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
    fun customResponse(
            @RequestHeader("Demo-Token") demoToken: String,
            @RequestBody user: UserDetail): ResponseEntity<DemoResponse> {

        var response: ResponseEntity<DemoResponse> = ResponseEntity.created(URI(""))
                .body(DemoResponse("accepted", HttpStatus.ACCEPTED.value()))

        if (demoToken == null || demoToken != "abc-1234567890") {
            response = ResponseEntity.badRequest()
                    .body(DemoResponse("Unauthorized", HttpStatus.UNAUTHORIZED.value()))
        }

        if (response.body?.responseCode == HttpStatus.ACCEPTED.value()) {

            if (user.username.length == 0) {
                response = ResponseEntity.badRequest()
                        .body(DemoResponse("bad request", HttpStatus.BAD_REQUEST.value()));

            } else {
                response = ResponseEntity
                        .created(URI(""))
                        .body(DemoResponse("success", HttpStatus.CREATED.value()));
            }
        }

        return response
    }

    @GetMapping(path = arrayOf("/path-variable/{pathVar}"),
            produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
    fun pathVariable(
            @RequestHeader("Demo-Token") demoToken: String,
            @PathVariable("pathVar") pathVar: String): ResponseEntity<Any> {

        var response: ResponseEntity<Any> = ResponseEntity.created(URI(""))
                .body(DemoResponse("accepted", HttpStatus.ACCEPTED.value()))

        if (demoToken == null || demoToken != "abc-1234567890") {
            response = ResponseEntity.badRequest()
                    .body(DemoResponse("Unauthorized", HttpStatus.UNAUTHORIZED.value()))
        }

        //response = response as ResponseEntity<Any>
        var demoResponse = response.body as DemoResponse

        if (demoResponse.responseCode == HttpStatus.ACCEPTED.value()) {
            var userDetail = UserDetail(
                    "jdoe",
                    "John Doe",
                    "male",
                    29,
                    SimpleDateFormat("dd-MM-yyyy").parse("31-01-1990"))

            if (pathVar != "jdoe") {
                response = ResponseEntity.status(HttpStatus.NOT_FOUND)
                        .body(DemoResponse("bad request", HttpStatus.BAD_REQUEST.value()));

            } else {
                response = ResponseEntity.ok().body(userDetail);
            }
        }

        return response
    }
}