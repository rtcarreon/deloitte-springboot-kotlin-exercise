package com.deloitte.demo

class DemoResponse (_message: String, _responseCode: Int) {
    var message: String = _message
    var responseCode: Int = _responseCode
}